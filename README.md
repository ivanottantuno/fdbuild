# FDBuild

Straightforward meta-build system to configure, build and install multiple projects at once.

### Status
Project start was at the end of December 2017, so there are still bugs and fundamental functionality might still be up to change.

#### Available features

* source handling, configuration, building and installing with one single command,
* projects can be hierarchically structured in subprojects,
* description of projects with simple YAML settings files following the natural directory hierarchy of projects,
  * looks up settings values also in project hierarchy,
  * use variables in settings files to reference other settings values,
  * set hooks to be executed before or after configuration, build or installation,
* dynamic building of projects and subprojects relative to current working directory,
* plugin system to support multiple source handling, configuration and build tools:
  * Source control-systems: Git
  * Build configuration: CMake, GNU Autoconf, Meson
  * Build systems: Make, Ninja
* templating engine to create with one single command full project hierarchies with pre-defined values,
* templates for popular open source projects included.

#### Current feature goals:

* Support to include a template in another one.
* Increase number of included templates of popular open source projects.
* Per project configurability if an error stops the process or if it is continued.
* Overview of succesful and failed projects at process end.
* Output progress percentage when build system supports it.

### Installation
Clone this repo, add it to your `$PATH`.

Additionally you need Python3 (>3.4) and PyYAML. For this issue on Debian based systems:

```
sudo apt install python3 python3-yaml
```

### Quick start
#### Clean sheet
Create some directory, go into it and run `fdbuild --init` to initialize a FDBuild project in this directory. This creates a default FDBuild settings file `fdbuild.yaml` in the directory.

Change the settings file values to your liking and run `fdbuild` from the same directory again.

#### Template
Pick a template from the [templates](templates) directory and run `fdbuild --init-with-template <templatename>` to deploy the template in form of a new directory `<templatename>` in your current working directory.

### Settings files
#### Hierarchy lookup
A subproject always adheres to the nearest available settings value in its hierarchy. For example take the following project tree:

```
PRJ_A
│   fdbuild.yaml
│
└───PRJ_B
|   │   fdbuild.yaml
|   └───PRJ_C
|   │       fdbuild.yaml
|   └───PRJ_D
|           fdbuild.yaml
│
└───PRJ_E
        fdbuild.yaml
```
To query a specific settings value for project `PRJ_C`, FDBuild will first look in `PRJ_A/PRJ_B/PRJ_C/fdbuild.yaml`. If this settings file defines the value, FDBuild will use it on project `PRJ_C`.

Otherwise FDBuild will look into `PRJ_A/PRJ_B/fdbuild.yaml`. And if this settings file also does not provide the value, FDBuild will look into `PRJ_A/fdbuild.yaml`.

Likewise FDBuild will for a settings value of project `PRJ_D` first check its settings file `PRJ_A/PRJ_B/PRJ_D/fdbuild.yaml` and afterwards as well the settings file `PRJ_A/PRJ_B/fdbuild.yaml` and next `PRJ_A/fdbuild.yaml`.

For project `PRJ_E` FDBuild will first look into `PRJ_A/PRJ_E/fdbuild.yaml`,
and if the value is not found there, look into `PRJ_A/fdbuild.yaml`.

If FDBuild is run in some directory the hierarchy unfolds recursively according to the `projects` lists in the working directory and all adjacent directories. For example assume that in the above tree FDBuild is run from directory `PRJ_B`, then the settings file of `PRJ_A` will considered if and only if `PRJ_B` is listed in the `projects` entry of settings file `PRJ_A/fdbuild.yaml`.

Assume further, that the parent directory of `PRJ_A` does not feature a FDBuild settings file listing `PRJ_A` as one of its projects. Then the lookup will stop here and `PRJ_A` becomes the toplevel directory of this project hierarchy defined through the settings files.

#### References
Besides common YAML syntax you can use the following syntax for referencing other entries in the same settings file or in another settings file within the project hierarchy: `@{path_to_other_entry}` If you need to use the sequence `@{` without declaring a reference you have to write `@@{`.

The path to the other reference follows a certain syntax as well: a section or entry in a settings file is denoted by one leading `$`, project hierarchies are divided by `/`. Some examples:

* `$section$entry` - take value from same settings file at labels `section: entry`
* `prjA/prjB$entry` - take value from label `entry` in settings file of project `prjA/prjB` relative to the settings file of the reference
* `/prjC/prjD$entry` - take value from label `entry` in the settings file of project `prjC/prjD`, whereas `prjC` is a subproject at the toplevel of the project hierarchy

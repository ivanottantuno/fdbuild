#!/usr/bin/env python

# ---------------------------------------------------------------------
# FDBuild
# Copyright © 2021 Roman Gilg
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# ---------------------------------------------------------------------
"""Initiates KDE structure and adapts it to KWinFT template usage.
"""

import os, shutil

def main():
    src_path = os.path.dirname(os.path.realpath(__file__))
    dst_path = os.getcwd()
    print("Copying 'meson-cross-file' from:\n  {}\nto:\n  {}".format(src_path, dst_path))
    shutil.copy(os.path.join(src_path, 'meson-cross-file'), dst_path)

if __name__ == "__main__":
    main()

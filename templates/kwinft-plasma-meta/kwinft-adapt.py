#!/usr/bin/env python

# ---------------------------------------------------------------------
# FDBuild
# Copyright © 2021 Roman Gilg
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# ---------------------------------------------------------------------
"""Initiates KDE structure and adapts it to KWinFT template usage.
"""

import fileinput, os, subprocess, yaml

def main():
    print("Switching to the kde subdirectory.")
    os.chdir('kde')

    print("Calling fdbuild --only-structure --noconfirm")
    ret = subprocess.call(['fdbuild', '--only-structure', '--noconfirm'])
    if ret != 0:
        return ret

    print("Disabling kde structurizer.")
    settgs = 'fdbuild.yaml'
    with open(settgs, 'r') as file:
        content = yaml.safe_load(file)
        content['structure']['enabled'] = False

    with open(settgs, 'w') as file:
        yaml.dump(content, file)

    print("Commenting out kscreen entry.")
    for line in fileinput.input(settgs, inplace = 1):
        line = line.replace("- kscreen\n", "#- kscreen\n")
        print(line, end='')


if __name__ == "__main__":
    main()

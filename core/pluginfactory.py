#!/usr/bin/env python3

# ---------------------------------------------------------------------
# FDBuild
# Copyright © 2018 Roman Gilg
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
# ---------------------------------------------------------------------

import importlib.util
from core.exceptions import *


def import_module(plugin_path):
    module_path = 'plugins.' + plugin_path
    spec = importlib.util.find_spec(module_path)
    if spec is None:
        msg = "Plugin " + plugin_path + " not found."
        raise InitError(None, msg)

    module = importlib.util.module_from_spec(spec)
    spec.loader.exec_module(module)
    return module


def get_plugin(plugin_path, *args):
    module = import_module(plugin_path)

    try:
        return module.Plugin(args)
    except Exception as error:
        # TODO: this hides error details. Just raise error instead or make it more clear what's happening.
        raise InitError(None, "Could not create plugin '%s': %s" %(plugin_path, str(error)))


def get_stage(name, step):
    assert step

    plugin_path = 'stages.' + name
    return get_plugin(plugin_path, step)


def get_step(stage, name, step):
    plugin_path = 'steps.' + stage + '.' + name
    return get_plugin(plugin_path, step)


def create_structure(name, owner):
    return get_step('structure', name, owner)

def create_source(name, owner):
    return get_step('source', name, owner)

def create_configure(name, owner):
    return get_step('configure', name, owner)

def create_build(name, owner):
    return get_step('build', name, owner)
